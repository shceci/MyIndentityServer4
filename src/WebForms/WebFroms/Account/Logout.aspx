﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logout.aspx.cs" Inherits="WebForms.Account.Logout" %>

<!DOCTYPE html>

<html lang="en-us">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0,shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="icon" href="../Images/icons/favicon.ico" />
</head>
<body>
    <form id="form1" runat="server" class="form-inline">
        <div>
        </div>
    </form>
</body>
</html>
<script type="text/javascript">
    logout();

    function logout() {
        const endSessionUrl = 'https://localhost:5001/connect/endsession';
        const idTokenHint = localStorage.getItem('id_token'); // 获取存储的id_token

        // 构建登出请求URL
        const logoutUrl = `${endSessionUrl}?id_token_hint=${encodeURIComponent(idTokenHint)}` +
            `&post_logout_redirect_uri=https%3A%2F%2Fyourapp.com%2Fhome%2Findex`;

        // 清除本地存储的令牌
        localStorage.removeItem('id_token');
        sessionStorage.removeItem('id_token');

        // 重定向到登出URL
        window.location.href = logoutUrl;
    }
</script>
