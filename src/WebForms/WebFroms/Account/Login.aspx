﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebForms.Account.Login" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="../Images/icons/favicon.ico">
    <title>To do list</title>

    <!-- Bootstrap core CSS -->
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="../Content/flags/famfamfam-flags.css" rel="stylesheet" />
    <%--<link href="../Css/main.css" rel="stylesheet" />--%>
    <script src="../Scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/bootstrap.min.js" type="text/javascript"></script>
    <style>
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #303641;
            color: #C1C3C6
        }
    </style>

</head>
<body>
    <div class="container col-lg-4 col-lg-offset-4">
        <a href="@Url.Action("Login", "Account")">Login</a>
    </div>
    <div class="clearfix"></div>
    <br>
    <br>
    <!--footer-->
    <div class="site-footer login-footer">
        <div class="container">
            <div class="copyright clearfix text-center">
                <p><b>@Hi-P</b></p>

            </div>
        </div>
    </div>
</body>
</html>
