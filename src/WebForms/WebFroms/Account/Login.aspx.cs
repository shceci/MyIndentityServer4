﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.IdentityModel.Tokens;
using Microsoft.IdentityModel.JsonWebTokens;
using System.Security.Cryptography;
using System.Text;

namespace WebForms.Account
{
    public partial class Login : System.Web.UI.Page
    {
        public const string SessionID = "SessionID";
        public const string SessionUserName = "SessionUserName";
        public const string SSOPassport = "SSOPassport";
        public const string SSOAppID = "SSOAppID";

        protected void Page_Load(object sender, EventArgs e)
        {
            string authorizationUrl = "https://localhost:5001/connect/authorize";
            string redirectUri = "https://localhost:44379/account/callback";
            string clientId = "webforms";
            string scope = "openid profile api1";
            string nonce = Guid.NewGuid().ToString();

            // 生成code_verifier和code_challenge
            string codeVerifier = GenerateCodeVerifier();
            string codeChallenge = GenerateCodeChallenge(codeVerifier);
            // 存储code_verifier以供后续使用
            HttpContext.Current.Session.Add("CodeVerifier", codeVerifier);

            string url = $"{authorizationUrl}?" +
                         $"client_id={HttpUtility.UrlEncode(clientId)}&" +
                         $"redirect_uri={HttpUtility.UrlEncode(redirectUri)}&" +
                         $"response_type=code&" +
                         $"scope={HttpUtility.UrlEncode(scope)}&" +
                         $"nonce={HttpUtility.UrlEncode(nonce)}&" +
                         $"state={HttpUtility.UrlEncode("state value here")}&" +
                         $"code_challenge={HttpUtility.UrlEncode(codeChallenge)}&" +
                         $"code_challenge_method=S256"; // 指定使用S256方法
            Response.Redirect(url);
        }

        private string GenerateCodeVerifier()
        {
            // 生成一个随机的code_verifier字符串
            byte[] randomBytes = new byte[32];
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomBytes);
            }
            string codeVerifier = Convert.ToBase64String(randomBytes).TrimEnd('+', '/').Replace("=", "");
            return codeVerifier;
        }

        private string GenerateCodeChallenge(string codeVerifier)
        {
            // 计算code_verifier的SHA-256哈希值
            byte[] hash = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(codeVerifier));
            // Base64 URL编码
            string base64UrlEncoded = Base64UrlEncoder.Encode(hash);
            return base64UrlEncoded;
        }


        public static bool CheckLogin(string SessionID, string remark = "")
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(ConfigurationManager.AppSettings["SSOPassportApi"])
            };

            var requestUri = $"?userAuthSessionID={SessionID}&remark={remark}";

            try
            {
                var resp = httpClient.GetAsync(requestUri).Result;
                resp.EnsureSuccessStatusCode();
                return resp.Content.ReadAsAsync<bool>().Result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return false;
        }

        private string SsoLoginResult(string fullReturnUrl)
        {
            FormsAuthentication.SignOut();
            return $"{ConfigurationManager.AppSettings[SSOPassport]}?" +
                $"appID={ConfigurationManager.AppSettings[SSOAppID]}&" +
                $"returnUrl={fullReturnUrl}";
        }
    }
}