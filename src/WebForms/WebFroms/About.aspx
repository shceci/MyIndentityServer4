﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebForms.About" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%
        if (HttpContext.Current?.User?.Identity?.IsAuthenticated == true)
        {
            foreach (var item in ((System.Security.Claims.ClaimsIdentity)HttpContext.Current?.User?.Identity).Claims)
            {
                claims.InnerHtml += $"<dt>{item.Type}</dt><dd>{item.Value}</dd>";
            }
        }

    %>
    <h2><%: Title %>.</h2>
    <h3>Your application description page.</h3>
    <p>Use this area to provide additional information.</p>
    <dl runat="server" id="claims"></dl>
    <a href="../Account/Logout">登出</a>
    <a href="../Account/Login">登入</a>
</asp:Content>
