﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace WebForms
{
    public class Global : System.Web.HttpApplication
    {
        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    if (FormsAuthentication.CookieMode == HttpCookieMode.UseDeviceProfile)
        //    {
        //        HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
        //        //HttpCookie authCookie = Request.Cookies["idsrv2"];
        //        if (authCookie != null && authCookie.Value != null)
        //        {
        //            string encryptedTicket = authCookie.Value;
        //            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(encryptedTicket);
        //            if (ticket != null)
        //            {
        //                Response.Write("Ticket Decrypted Successfully.");
        //            }
        //        }
        //    }
        //}

        protected void Application_PostAuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Write($"Authenticated: {HttpContext.Current.User.Identity.Name}");
            }
            else
            {
                Response.Write("Not Authenticated");
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            // 在应用程序启动时运行的代码
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}