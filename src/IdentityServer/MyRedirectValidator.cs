﻿using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer
{
    /// <summary>
    /// 无论返回地址是否在列表中，都允许。
    /// </summary>
    public class MyRedirectValidator : IRedirectUriValidator
    {
        public Task<bool> IsPostLogoutRedirectUriValidAsync(string requestedUri, Client client)
        {
            return Task.FromResult(true);//不验证返回登出地址是否在列表中
            //foreach (String uri in client.PostLogoutRedirectUris)
            //{
            //    string pattern = string.Concat(@"^", uri);
            //    Match m = Regex.Match(requestedUri, pattern, RegexOptions.IgnoreCase);
            //    if (m.Success)
            //    {
            //        return Task.FromResult(true);
            //    }
            //}
            //return Task.FromResult(false);
        }

        public Task<bool> IsRedirectUriValidAsync(string requestedUri, Client client)
        {
            return Task.FromResult(true);//不验证返回地址是否在SSO的列表中
            //foreach (String uri in client.RedirectUris)
            //{
            //    string pattern = string.Concat(@"^", uri);
            //    Match m = Regex.Match(requestedUri, pattern, RegexOptions.IgnoreCase);
            //    if (m.Success)
            //    {
            //        return Task.FromResult(true);
            //    }
            //}
            //return Task.FromResult(false);
        }
    }
}