﻿//using HiP.Infrastructure.LDAP;
using IdentityServer.CustomValidator;
using IdentityServerHost.Quickstart.UI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace IdentityServer
{
    public class Startup
    {
        public IWebHostEnvironment Environment { get; }
        public IConfiguration Configuration { get; private set; }
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // uncomment, if you want to add an MVC-based UI
            services.AddControllersWithViews();
            //services.ConfigureNonBreakingSameSiteCookies();
            var builder = services.AddIdentityServer(options =>
            {
                // see https://identityserver4.readthedocs.io/en/latest/topics/resources.html
                options.EmitStaticAudienceClaim = true;
            })
                .AddInMemoryIdentityResources(Config.IdentityResources)
                .AddInMemoryApiScopes(Config.ApiScopes)
                .AddInMemoryClients(Config.Clients)
                //启用自定义验证及Profile，取代示例用户
                //.AddResourceOwnerValidator<LoginValidator>()//自定义用户校验

                .AddTestUsers(TestUsers.Users)    //使用示例用户
                .AddProfileService<ProfileService>()  //自定义Profile
                .AddCorsPolicyService<MyCorsPolicyService>()  //使用自定义Cors校验
                .AddRedirectUriValidator<MyRedirectValidator>() //使用自定义RedirectUri验证
                .AddDeveloperSigningCredential()    //生产环境应该使用如下方式
                //.AddSigningCredential(new X509Certificate2(Path.Combine(Directory.GetCurrentDirectory(),
                //        Configuration["Certificates:CerPath"]),
                //    Configuration["Certificates:Password"]))
                ;
            ;
        }

        public void Configure(IApplicationBuilder app)
        {
            //if (Environment.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            // uncomment if you want to add MVC
            app.UseStaticFiles();
            app.UseRouting();
            app.UseCookiePolicy();
            app.UseIdentityServer();

            // uncomment, if you want to add MVC
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
