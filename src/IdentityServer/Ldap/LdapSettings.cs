﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HiP.Infrastructure.LDAP
{
    public class LdapSettings
    {
        public string Domain { get; set; }
        public string ServerAddress { get; set; }
        public int ServerPort { get; set; }
        public int ServerPortSsl { get; set; }
        public string BaseDn { get; set; }
        /// <summary>
        /// 域管理员账号用户名，如果只是验证登录用户，不对域做修改，可以就是登录用户名
        /// </summary>
        public string RootUserAccount { get; set; }

        /// <summary>
        /// //域管理员账号密码，如果只是验证登录用户，不对域做修改，可以就是登录用户的密码
        /// </summary>
        public string RootUserPassword { get; set; } 

        public string DomainDn { get; set; }
    }
}
