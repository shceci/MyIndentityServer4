﻿using Novell.Directory.Ldap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HiP.Infrastructure.LDAP
{
    public static class LdapEntryExtensions
    {
        public static Guid GetGuid(this LdapEntry entry)
        {
            return new Guid(entry.GetAttribute("objectGUID").ByteValue);
        }

        public static string GetString(this LdapEntry entry, string attrbuiteName)
        {
            try
            {
                return entry.GetAttributeSet().Any(u => u.Key == attrbuiteName) ? entry.GetAttribute(attrbuiteName).StringValue : "";
            }
            catch (Exception)
            {
                return "";
                throw;
            }
        }
    }
}
