﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using HiP.Infrastructure.LDAP;
using Microsoft.Extensions.Options;

namespace IdentityServer.CustomValidator
{
    /// <summary>
    /// 自定义用户登录校验
    /// </summary>
    public class LoginValidator : IResourceOwnerPasswordValidator
    {
        private LdapSettings _ldapSettings;
        public LoginValidator(IOptions<LdapSettings> setting)
        {
            _ldapSettings = setting.Value;
        }

        /// <summary>
        /// 登录用户校验
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            var help = new ADHelper();

           var ldapresult= help.VerifyPassword(context.UserName, context.Password);
            //根据context.UserName和context.Password与数据库的数据做校验，判断是否合法
            if (ldapresult)
            {
                var entry = help.GetByIdentity(context.UserName);

               var claims= new Claim[]
                {
                    new Claim("UserId", entry.GetGuid().ToString()),
                    new Claim(JwtClaimTypes.Name, entry.GetString("displayName")),
                    new Claim("Account", entry.GetString("sAMAccountName")),
                    new Claim("EmployeeCode", entry.GetString("pager")),
                    new Claim("Department", entry.GetString("department")),
                    new Claim("Position", entry.GetString("title")),
                    new Claim("CompanyCode", entry.GetString("company")),
                    new Claim("Description", entry.GetString("description")),
                    new Claim("Mobile", entry.GetString("mobile")),
                    new Claim("ExtensionNo", entry.GetString("telephoneNumber")),
                    new Claim(JwtClaimTypes.Email, entry.GetString("mail"))
                   
                };

                context.Result = new GrantValidationResult(
                 subject: entry.GetGuid().ToString(),
                 authenticationMethod: "custom",
                 claims: claims);
            }
            else
            {
                //验证失败
                context.Result = new GrantValidationResult(TokenRequestErrors.InvalidGrant, "账号密码错误");
            }
        }
        /// <summary>
        /// 可以根据需要设置相应的Claim
        /// </summary>
        /// <returns></returns>
        private Claim[] GetUserClaims()
        {
            return new Claim[]
            {
                new Claim("UserId", 1.ToString()),
                new Claim(JwtClaimTypes.Name,"test"),
                new Claim(JwtClaimTypes.GivenName, "jaycewu"),
                new Claim(JwtClaimTypes.FamilyName, "yyy"),
                new Claim(JwtClaimTypes.Email, "test@qq.com"),
                new Claim(JwtClaimTypes.Role,"admin")
            };
        }
    }
}