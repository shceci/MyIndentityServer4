﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email(),
                new IdentityResources.Address(),
                new IdentityResources.Phone()
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("api1", "My API")
            };

        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                new Client
                {
                    ClientId = "client",

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    // scopes that client has access to
                    AllowedScopes = { "api1" }
                },
                // wpf client, password grant
                new Client
                {
                    ClientId = "wpf client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    ClientSecrets =
                    {
                        new Secret("wpf secrect".Sha256())
                    },
                    AllowedScopes = {
                        "api1",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Email,
                        IdentityServerConstants.StandardScopes.Address,
                        IdentityServerConstants.StandardScopes.Phone,
                        IdentityServerConstants.StandardScopes.Profile }
                },
                // interactive ASP.NET Core MVC client
                new Client
                {
                    ClientId = "mvc",
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.Code,
                    RedirectUris = { "https://localhost:5005/signin-oidc" },// 登入成功时返回客户端的地址
                    PostLogoutRedirectUris = { "https://localhost:5005/signout-callback-oidc" },// 登出时返回客户端的地址
                    FrontChannelLogoutUri = "https://localhost:5005/signout-callback-oidc", //统一登出
                    AllowOfflineAccess = true,    //允许为长时间的API访问请求刷新令牌
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api1"
                    }
                },
                new Client
                {
                    ClientId = "mvc3",
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.Code,
                    RedirectUris = { "https://localhost:5006/signin-oidc" },// 登入成功时返回客户端的地址
                    PostLogoutRedirectUris = { "https://localhost:5006/signout-callback-oidc" },// 登出时返回客户端的地址
                    
                    FrontChannelLogoutUri = "https://localhost:5006/signout-callback-oidc", //统一登出
                    AllowOfflineAccess = true,    //允许为长时间的API访问请求刷新令牌
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api1"
                    }
                },
                // JavaScript Client
                new Client
                {
                    ClientId = "js",
                    ClientName = "JavaScript Client",
                    AllowedGrantTypes = GrantTypes.Code,
                    RequireClientSecret = false,
                    RedirectUris =           { "https://localhost:5003/callback.html" },
                    PostLogoutRedirectUris = { "https://localhost:5003/index.html" },
                    AllowedCorsOrigins =     { "https://localhost:5003" },  //可在Startup中使用AddCorsPolicyService略过该验证
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api1"
                    }
                },
                // WebForms Client
                new Client
                {
                    ClientId = "webforms",
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowedGrantTypes = GrantTypes.Code,
                    RedirectUris = { "https://localhost:44379/signin-oidc" },// 登入成功时返回客户端的地址
                    PostLogoutRedirectUris = { "https://localhost:44379/signout-callback-oidc","https://localhost:44379","https://localhost:44379/about.aspx"   },// 登出时返回客户端的地址
                    FrontChannelLogoutUri = "https://localhost:44379/account/Logout.aspx",  //统一登出
                    AllowOfflineAccess = true,    //允许为长时间的API访问请求刷新令牌
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api1"
                    }
                },
                // WebForms2 Client
                new Client
                {
                    ClientId = "webforms2",
                    ClientName = "Webforms Client",
                    RequireClientSecret = false,
                    AllowedGrantTypes = GrantTypes.Implicit,
                    ClientUri = "https://localhost:44379",
                    AllowAccessTokensViaBrowser = true,
                    //RequireConsent = false, //是否需要用户点击同意，这里需要设置为 false，不然客户端静默刷新不可用
                    //AllowRememberConsent = true,
                    RedirectUris =           { "https://localhost:44379" },
                    PostLogoutRedirectUris = { "https://localhost:44379" },
                    FrontChannelLogoutUri = "https://localhost:44379/logout",   //统一登出

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api1"
                    }
                }
            };
    }
}